# west
execute as @a at @s if entity @s[y_rotation=45..135] run place template bumpercar:car ~-3 ~-1 ~2 counterclockwise_90
# east
execute as @a at @s if entity @s[y_rotation=-134..-45] run place template bumpercar:car ~3 ~-1 ~-2 clockwise_90
# north
execute as @a at @s if entity @s[y_rotation=135..-135] run place template bumpercar:car ~-2 ~-1 ~-3 none
# south
execute as @a at @s if entity @s[y_rotation=-44..44] run place template bumpercar:car ~2 ~-1 ~3 180
